# TestMod

This is a universal testing mod for MelonLoader, designed to piss off its obfuscation detection.

See [this post on my blog](https://www.nexisonline.net/index.php/2021/04/11/melonloader-sold-out/) for more information.

This is **not** intended to be functional.

## Compiling

1. Install Python >=3.8 and .NET Core 4.7.2
1. `python -m pip install -U pip`
1. `pip install -U vnm`
1. `vnm i`
1. `.venv/Scripts/activate.bat`
1. `python BUILD.py`
