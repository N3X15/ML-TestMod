# This performs setup tasks, then builds the project.
import re
from pathlib import Path

import steamclient

from buildtools.maestro import BuildMaestro
from buildtools.maestro.fileio import ReplaceTextTarget, CopyFileTarget
from buildtools.maestro.shell import CommandBuildTarget
from buildtools.http import DownloadFile
from buildtools import log, os_utils

PROJDIR: Path = Path(__file__).parent
STEAMAPPID: int = 438100
VRCHATDIR: Path = None

def all_games() -> Path:
    user = steamclient.get_users()[0]
    appids = {}
    for library in steamclient.get_libraries():
        lib = Path(library)
        #print(str(lib))
        if (lib / 'steamapps' / 'common').is_dir():
            lib = lib / 'steamapps' / 'common'
        for appidfile in lib.glob('*/steam_appid.txt'):
            try:
                appid: int = int(appidfile.read_text().split('\n')[0])
                appids[appid] = appidfile.parent
                print(f'{appid}\t{appidfile.parent}')
            except:
                print(f"{appidfile} is borked.")
    return appids

CONFUSER = Path('lib/confuser-cli/Confuser.CLI.exe')
os_utils.ensureDirExists(str(CONFUSER.parent))
if not CONFUSER.is_file():
    cfzip = Path('lib/ConfuserEx-CLI-1.5.0.zip')
    if not cfzip.is_file():
        DownloadFile('https://github.com/mkaring/ConfuserEx/releases/download/v1.5.0/ConfuserEx-CLI.zip', cfzip)
    os_utils.decompressFile(str(cfzip), str(CONFUSER.parent))

bm = BuildMaestro()

REPLACE = {
    r'@@PROJDIR@@': str(PROJDIR).replace('\\', '\\\\'),
    r'@@VRCHATDIR@@': str(all_games()[STEAMAPPID]).replace('\\', '\\\\'),
}

replacements = [
    bm.add(ReplaceTextTarget(str(PROJDIR / 'TestMod.crproj'), str(PROJDIR / 'TestMod.crproj.in'), REPLACE)).target,
    bm.add(ReplaceTextTarget(str(PROJDIR / 'TestMod.csproj'), str(PROJDIR / 'TestMod.csproj.in'), REPLACE)).target,
]
cmd=[
    'dotnet',
    'build',
    '--no-restore',
    '-c','Release',
    'TestMod.csproj']
outfile = str(Path('bin/Release/net472/TestMod.dll'))
infiles = [
    'TestMod.cs'
]
unobfuscated = bm.add(CommandBuildTarget([outfile], infiles, cmd, dependencies=replacements)).provides()[0]

cmd=[
    str(CONFUSER),
    '-n',
    'TestMod.crproj']
outfile = str(Path('Confused/bin/Release/net472/TestMod.dll'))
infiles = [
    'TestMod.cs'
]
obfuscated = bm.add(CommandBuildTarget([outfile], infiles, cmd, dependencies=[unobfuscated])).provides()[0]
bm.add(CopyFileTarget('dist/TestMod.dll', outfile, dependencies=[obfuscated]))
bm.as_app()
