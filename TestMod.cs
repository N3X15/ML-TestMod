﻿using System;
using MelonLoader;
using System.Runtime.InteropServices;


[assembly: MelonInfo(typeof(TestMod.TestMod), "TestMod", "0.0.1", "N3X15", "https://gitlab.com/N3X15/ML-TestMod")]
[assembly: MelonGame(null, null)]

namespace TestMod
{
    public class TestMod : MelonMod
    {
        [DllImport("User32.dll", CharSet = CharSet.Unicode)]
        private static extern int MessageBox(IntPtr nWnd, string text, string title, uint type);

        const uint MB_OK = 0;
        const uint MB_ICONEXCLAMATION = 0x30;
        const uint TOPMOST = 0x40000;

        public override void OnApplicationStart()
        {
            string date = DateTime.Now.ToString();
            string omg = "It works!";
            MessageBox(IntPtr.Zero, omg+"\n"+date, "TestMod", MB_OK | MB_ICONEXCLAMATION | TOPMOST);
        }
    }
}
